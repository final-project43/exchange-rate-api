package com.finalProject.exchangeApi.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.finalProject.exchangeApi.repository.ExchangeRepository;
import com.finalProject.exchangeApi.repository.models.ExchangeRate;
import com.finalProject.exchangeApi.service.dto.SwapResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExchangeService {

   private final ExchangeRepository exchangeRepository;

   @Autowired
   public ExchangeService(ExchangeRepository exchangeRepository) {
      this.exchangeRepository = exchangeRepository;
   }

   public ResponseEntity<SwapResponse> getExchangeRate(String from, String to, BigDecimal initialAmount) {

      if (!from.equals("USD") && !to.equals("USD")) {
         return setErrorResponse("Can´t swap '" + from + "' to '" + to + "' !");
      }
      if (from.equals("USD") && to.equals("USD")) {
         return setResponse("USD", initialAmount);
      }
      if (from.equals("USD")) {
         Optional<ExchangeRate> exchangeRateOptional = exchangeRepository.findByIso(to);
         if (exchangeRateOptional.isPresent()) {
            log.info("Swapping from USD to " + to);
            BigDecimal totalAmount = initialAmount.multiply(exchangeRateOptional.get().getAmount());
            return setResponse(to, totalAmount);
         } else {
            return setErrorResponse("The currency '" + to + "' not found.");
         }
      } else {
         Optional<ExchangeRate> exchangeRateOptional = exchangeRepository.findByIso(from);
         if (exchangeRateOptional.isPresent()) {
            log.info("Swapping from " + from + "to USD");
            BigDecimal totalAmount = initialAmount.divide(exchangeRateOptional.get().getAmount(), 2, RoundingMode.DOWN);
            return setResponse(to, totalAmount);
         } else {
            return setErrorResponse("The currency '" + from + "' not found.");
         }
      }
   }

   private ResponseEntity<SwapResponse> setErrorResponse(String reason) {
      SwapResponse swapResponse = new SwapResponse();
      swapResponse.setError(reason);
      return new ResponseEntity<>(swapResponse, HttpStatus.NOT_FOUND);
   }

   private ResponseEntity<SwapResponse> setResponse(String iso, BigDecimal amount) {
      SwapResponse swapResponse = new SwapResponse();
      swapResponse.setCurrency(iso);
      swapResponse.setAmount(amount);
      return new ResponseEntity<>(swapResponse, HttpStatus.OK);
   }

}
