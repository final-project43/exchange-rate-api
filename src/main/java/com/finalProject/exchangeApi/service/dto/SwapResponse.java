package com.finalProject.exchangeApi.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SwapResponse {

    private String currency;

    private BigDecimal amount;

    private String error;

}
