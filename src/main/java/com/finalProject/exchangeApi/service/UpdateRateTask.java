package com.finalProject.exchangeApi.service;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.finalProject.exchangeApi.repository.ExchangeRepository;
import com.finalProject.exchangeApi.repository.models.ExchangeRate;
import com.finalProject.exchangeApi.service.dto.RateRS;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UpdateRateTask {

   private final WebClient webClient;

   private final ExchangeRepository exchangeRepository;

   @Autowired
   public UpdateRateTask(WebClient webClient, ExchangeRepository exchangeRepository) {
      this.webClient = webClient;
      this.exchangeRepository = exchangeRepository;
   }

   @Scheduled(fixedDelayString = "6000")
   public void updateRate() {
      List<ExchangeRate> exchangeRates = exchangeRepository.findAll();

      for (ExchangeRate exchangeRate : exchangeRates) {

         String currency = exchangeRate.getIso();

         String externalUrl = "https://api.exchangerate.host";
         URI uri = UriComponentsBuilder
               .fromUriString(externalUrl)
               .path("/convert")
               .queryParam("from", "USD")
               .queryParam("to", currency)
               .build()
               .toUri();

         RateRS rs = webClient.get().uri(uri).retrieve().bodyToMono(RateRS.class).block();

         try {
            exchangeRate.setAmount(rs.getResult());
            exchangeRepository.save(exchangeRate);
         } catch (Exception e) {
            log.error("Error updating exchange rates. Reason: " + e.getMessage());
         }
      }
   }

}
