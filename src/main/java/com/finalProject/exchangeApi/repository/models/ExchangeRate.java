package com.finalProject.exchangeApi.repository.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "exchange_rate")
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_exchange_rate")
    private Integer id;

    @Column(name = "iso")
    private String iso;

    @Column(name = "amount")
    private BigDecimal amount;

}
