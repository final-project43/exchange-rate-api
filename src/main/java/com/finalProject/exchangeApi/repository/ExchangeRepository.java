package com.finalProject.exchangeApi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.finalProject.exchangeApi.repository.models.ExchangeRate;

public interface ExchangeRepository extends JpaRepository<ExchangeRate, Integer> {

   Optional<ExchangeRate> findByIso(String iso);

}
