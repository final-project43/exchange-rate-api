package com.finalProject.exchangeApi.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.finalProject.exchangeApi.service.ExchangeService;
import com.finalProject.exchangeApi.service.dto.SwapResponse;

@RestController
@RequestMapping
public class ExchangeController {

   private final ExchangeService exchangeService;

   @Autowired
   public ExchangeController(ExchangeService exchangeService) {
      this.exchangeService = exchangeService;
   }

   @GetMapping("/exchangeRate")
   public ResponseEntity<SwapResponse> swapping(@RequestParam String from, @RequestParam String to, @RequestParam BigDecimal amount) {
      return exchangeService.getExchangeRate(from, to, amount);
   }

}
