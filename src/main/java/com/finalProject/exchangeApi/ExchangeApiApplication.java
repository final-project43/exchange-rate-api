package com.finalProject.exchangeApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ExchangeApiApplication {

   public static void main(String[] args) {
      SpringApplication.run(ExchangeApiApplication.class, args);
   }

}
