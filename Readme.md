# Exchange Rate API

API encargada de calcular los diferentes tipos de cambio según un monto y moneda. Solamente se puede convertir de una divisa hacia USD o viceversa.

## Flujo de cotización de divisas
1. El cliente envía una solicitud al sistema con una moneda inicial, una moneda de conversión y el monto a convertir.
2. El sistema consulta la cotización correspondiente en la tabla exchange_rate.
3. El sistema realiza el cálculo para convertir la divisa a USD o viceversa sobre el monto recibido.
4. El sistema responde al cliente el monto convertido y la divisa (ISO) en la cual esta expresada.

## Cálculo de la conversión de divisas

**De USD a otra divisa:**

result = USD amount * exchange_rate_local_amount

**De otra divisa a USD:**

result = local amount / exchange_rate_local_amount

## Endpoints

Sirve para calcular tipos de cambio de una moneda hacia otra según un monto.

`[GET] /exchangeRate?from=ARSto=USD&amount=12345`


**Query params:**

- **from** (String)
- **to** (String)
- **amount** (BigDecimal)

#### Response:

code : 200

```json
{
	"currency": "USD",
	"amount": 1234.50
}
```

Code: 404

``` json
{
	"error": "currency {currency} not found"
}
```